import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularCodemirror6Component } from './codemirror6-editor.component';

describe('AngularCodemirror6Component', () => {
  let component: AngularCodemirror6Component;
  let fixture: ComponentFixture<AngularCodemirror6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AngularCodemirror6Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AngularCodemirror6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
