import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  ViewChild
} from '@angular/core';
import { EditorState, Extension, StateEffect } from '@codemirror/state';
import { EditorView } from '@codemirror/view';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'codemirror6-editor',
  standalone: true,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: CodeMirrorComponent,
      multi: true
    },
  ],
  template: `<div #host></div>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeMirrorComponent implements AfterViewInit, OnDestroy, ControlValueAccessor {

  private _extensions: Extension[] = [];
  @Input()
  set extensions(extensions: Extension[]) {
    extensions.push(this.updateListener);
    this._extensions = extensions;
    this.editor?.dispatch({
      effects: StateEffect.reconfigure.of(extensions),
    });
  }

  editor: EditorView | null = null;
  private handleChange: (value: string) => void = () => {};
  private readonly updateListener = EditorView.updateListener.of((update) => {
    this.ngZone.run(() => {
      const value = update.state.doc.toString();
      this.handleChange(value);
    });
  });

  @ViewChild("host") private host!: ElementRef<HTMLElement>;

  constructor(private ngZone: NgZone) {}

  ngAfterViewInit(): void {
    const state = EditorState.create({
      extensions: this._extensions,
    });
    this.editor = new EditorView({
      state,
      parent: this.host.nativeElement,
    });
  }

  ngOnDestroy(): void {
    this.editor?.destroy();
  }

  writeValue(value: string) {
    this.editor?.dispatch({
      changes: {
        from: 0,
        to: this.editor.state.doc.length,
        insert: value,
      },
    });
  }

  registerOnChange(fn: (value: string) => void) {
    this.handleChange = fn;
  }

  registerOnTouched() {}
}
